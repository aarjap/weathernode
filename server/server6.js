/**
 * A friendly server that responds to URL requests by calling another service
 * Try: localhost:8086/Maryville, Missouri
**/
const http = require("http")
const weatherService = require('./WeatherService.js')
const port = 8086

const server = http.createServer(async (req, res) => {
    const route = await getRouteFromRequest(req)
    const city = await getCityFromRequest(req)
    if (city !== 'favicon.ico') {
        try {
            const ans = (route === 'temp') ? await weatherService.getTemp(city) : await weatherService.getHumidity(city)
            console.log(ans)
            res.writeHead(200, { 'Content-Type': 'text/html' })
            res.write("<!doctype html>")
            res.write("<html><head><title>Server6</title></head>")
            res.write("<body>")
            res.write("<h1>Hello Client - try one of these options</h1>")
            res.write("<h3>localhost:8086/temp/Maryville, Missouri</h3>")
            res.write("<h3>localhost:8086/humidity/Maryville, Missouri</h3>")
            res.write("<br/>")
            res.write("<h3>You made a " + route + " request for " + city + ".<h3>")
            res.write("<p>" + ans + "<p>")
            res.write("<p>Please try again.<p>")
            res.end("</body></html>")
        }
        catch (err) {
            res.writeHead(200, { 'Content-Type': 'text/html' })
            res.write("<!doctype html>")
            res.write("<html><head><title>Server6</title></head>")
            res.write("<body>")
            res.write("<h1>Hello Client - try one of these options</h1>")
            res.write("<h3>localhost:8086/temp/Maryville, Missouri</h3>")
            res.write("<h3>localhost:8086/humidity/Maryville, Missouri</h3>")
            res.write("<br/>")
            res.write("<p>Please try again.<p>")
            res.end("</body></html>")
        }
    }
})

async function getRouteFromRequest(request) {
    const { headers, method, url } = request
    const strippedURL = url.substring(1, url.length) // remove the slash at url[0]
    const route = strippedURL.substring(0, strippedURL.indexOf('/'))
    console.log(route)
    return route
}

async function getCityFromRequest(request) {
    const { headers, method, url } = request
    const strippedURL = url.substring(1, url.length) // remove the slash at url[0]
    const justCityPart = strippedURL.substring(1+strippedURL.indexOf('/'), strippedURL.length)
    const city = justCityPart.replace('%20', " ")
    console.log(city)
    return city
}

server.listen(port, (err) => {
    if (err) { return console.log('Error', err) }
    console.log(`Server listening at http://127.0.0.1:${port}`)
})

